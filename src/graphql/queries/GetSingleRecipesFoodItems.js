import { gql } from 'apollo-boost';

export default gql`
  query GetSingleRecipeFoodItems($nameOfItem: String!) {
    singleRecipeFoodItems(nameOfItem: $nameOfItem) {
        weightOfItem
    }
  }
`;