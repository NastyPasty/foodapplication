import { gql } from 'apollo-boost'

export default gql`
query GetAllRecipeNames {
  recipeNames{
    id
    recipeid
    recipeName
    timeToCook
    prepTime
    totalTime
    servings
    recipeTag
    file
  }
}`