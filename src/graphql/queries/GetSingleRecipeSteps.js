import { gql } from 'apollo-boost'

export default gql`
query GetRecipeRecipeSteps($recipeID: String!) {
    recipeSteps(recipeID:$recipeID){
    id
    recipeID
    stepInformation

  }
}`