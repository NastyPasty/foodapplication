import { gql } from 'apollo-boost'

export default gql`
query GetAllKithcenEquipment {
  kitchenEquipments{
    id
    kitchenEquipmentName
  }
}`