import { gql } from 'apollo-boost';

export default gql`
  query GetSingleRecipe($recipeId: ID!) {
    recipeNames(where: { id: $recipeId }) {
      id
      recipeid
      recipeName
      timeToCook
      prepTime
      totalTime
      servings
    }
  }
`;