import { gql } from 'apollo-boost'

export default gql`
query GetRecipeIngredients($recipeID: String!) {
  recipeIngredients(recipeID:$recipeID){
    id
    recipeID
    ingredientUnit 
    ingredientQuantity
    ingredientName
  }
}`