import { gql } from 'apollo-boost'

export default gql`
query GetAllFoodItems {
  foodItems{
    id
    nameOfItem,
    expirationDate,
    weightOfItem,
    unitOfMeasurement
  }
}`