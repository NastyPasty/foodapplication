import { gql } from 'apollo-boost'

export default gql`
query GetRecipeIngredients {
  recipeIngredients($recipeID){
    recipeID
    ingredientUnit
    ingredientQuantity
    ingredientName

  }
}`