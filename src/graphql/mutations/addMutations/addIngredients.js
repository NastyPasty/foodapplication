import gql from "graphql-tag"


export default gql`
mutation AddIngredients(
$recipeID: String!,
$ingredientUnit : String!,
$ingredientQuantity: String!,
$ingredientName: String!
)
{
  addIngredients(
    recipeID: $recipeID,
    ingredientUnit: $ingredientUnit,
    ingredientQuantity: $ingredientQuantity,
    ingredientName: $ingredientName) {
      id
    }
 
}`;