import gql from "graphql-tag"


export default gql`
mutation AddFoodItem(
$nameOfItem: String!,
$expirationDate: Date!,
$weightOfItem: Int!,
$unitOfMeasurement: String!
)
{
  addFoodItem(
    nameOfItem: $nameOfItem,
    expirationDate: $expirationDate,
    weightOfItem: $weightOfItem,
    unitOfMeasurement: $unitOfMeasurement,
    ) {
      id
    }
 
}`;