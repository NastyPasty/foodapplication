import gql from "graphql-tag"


export default gql`
mutation AddRecipeStep(
$recipeID: String!,
$stepInformation: String!
)
{
  addRecipeStep(
    recipeID: $recipeID,
    stepInformation: $stepInformation) {
      id
    }
 
}`;

