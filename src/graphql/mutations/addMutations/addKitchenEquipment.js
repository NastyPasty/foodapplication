import gql from "graphql-tag"


export default gql`
mutation AddKitchenEquipment(
$kitchenEquipmentName: String!)
{
  addKitchenEquipment(
    kitchenEquipmentName: $kitchenEquipmentName) {
      id
    }
 
}`;

