import { gql } from 'apollo-boost'

export default gql`
mutation AddRecipeName(
$recipeid: String!,
$recipeName: String!,
$timeToCook: Int!,
$prepTime: Int!,
$totalTime: Int!,
$servings: Int!,
$recipeTag: String!,
$file: Upload)
{
  addRecipeName(
    recipeid: $recipeid,
    recipeName: $recipeName,
    timeToCook: $timeToCook,
    prepTime: $prepTime,
    totalTime: $totalTime,
    servings: $servings,
    recipeTag: $recipeTag,
    file: $file) {
      id
    }
 
}`;