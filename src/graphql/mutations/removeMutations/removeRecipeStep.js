import gql from "graphql-tag"


export default gql`
mutation RemoveRecipeStep(
$id: String!)
{
  removeRecipeStep(
    id: $id) {
      id
    }
 
}`;