import gql from "graphql-tag"


export default gql`
mutation RemoveRecipeName(
$_id: String!)
{
  removeRecipeName(
    id: $_id) {
      id
    }
 
}`;
