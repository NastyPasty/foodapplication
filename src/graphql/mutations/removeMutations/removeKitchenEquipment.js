import gql from "graphql-tag"


export default gql`
mutation RemoveKitchenEquipment(
$_id: String!)
{
  removeKitchenEquipment(
    id: $_id) {
      id
    }
 
}`;

