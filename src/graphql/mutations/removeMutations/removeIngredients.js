import gql from "graphql-tag"


export default gql`
mutation RemoveIngredients(
$_id: String!)
{
  removeIngredients(
    id: $_id) {
      id
    }
 
}`;