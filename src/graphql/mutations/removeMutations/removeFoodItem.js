import gql from "graphql-tag"


export default gql`
mutation RemoveFoodItem(
$_id: String!)
{
  removeFoodItem(
    id: $_id) {
      id
    }
 
}`;

