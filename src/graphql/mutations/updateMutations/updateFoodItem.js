import gql from "graphql-tag"


export default gql`
mutation UpdateFoodItem(
$_id: String!,
$nameOfItem: String!,
$weightOfItem: Int!,
$unitOfMeasurement: String!,
$expirationDate: Date!)
{
  updateFoodItem(
    id: $_id,
    nameOfItem: $nameOfItem,
    weightOfItem: $weightOfItem,
    unitOfMeasurement: $unitOfMeasurement,
    expirationDate: $expirationDate
    ) {
      id
    }
 
}`;