import gql from "graphql-tag"


export default gql`
mutation UpdateIngredient(
$_id: String!
$ingredientUnit : String!,
$ingredientQuantity: String!,
$ingredientName: String!)
{
  updateIngredient(
    id: $_id
    ingredientUnit: $ingredientUnit,
    ingredientQuantity: $ingredientQuantity,
    ingredientName: $ingredientName) {
      id
    }
 
}`;