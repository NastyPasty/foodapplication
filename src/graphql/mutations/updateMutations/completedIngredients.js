import gql from "graphql-tag"


export default gql`
mutation CompletedFoodItem(
$nameOfItem: String!,
$weightOfItem: Int!,
$orignalWeight: Int!,
$unitOfMeasurement: String,)
{
  completedFoodItem(
    nameOfItem: $nameOfItem,
    weightOfItem:  $weightOfItem,
    orignalWeight: $orignalWeight
    unitOfMeasurement: $unitOfMeasurement
    ) {
      id
    }
 
}`;