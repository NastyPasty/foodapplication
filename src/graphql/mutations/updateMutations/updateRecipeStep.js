import gql from "graphql-tag"


export default gql`
mutation UpdateRecipeStep(
$id: String!
$stepInformation: String!)
{
  updateRecipeStep(
    id: $id
    stepInformation: $stepInformation) {
      id
    }
 
}`;