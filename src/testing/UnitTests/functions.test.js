const functions = require('./function');
const validate = require('../../Functions/validation')


test('Validate Kitchen Equipment Name Length return false', () => {
  const variable = "Above0";
  expect(validate(variable)).toEqual({ variable: false });
});

test('Validate Kitchen Equipment Name Length return true', () => {
  const variable = "";
  expect(validate(variable)).toEqual({ variable: true });
});

test('adds 2 + 2 to equal 4', () => {
  const two = 2;
  expect(functions.add(two, two)).toBe(4);
});


