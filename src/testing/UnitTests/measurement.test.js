const Measurement = require('../../components/RecipeIngredients/measurement');
const Quantity = require('../../components/RecipeIngredients/quantity');
const unit = 'Gram';
const quantity = new Quantity('1000').multiply(2, 'grams');
const measurement = new Measurement(quantity, unit).convertUnits();

test('measurmeent going up from grams to kg', () => {
  expect(measurement.unit).toEqual('Kg');
});


test('measurmeent going up from 1000 grams to 1 kg', () => {
  expect(measurement.quantity.wholeNumber).toEqual(2);
});