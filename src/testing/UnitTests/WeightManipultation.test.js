const { ConvertWeightToLowestForm, ConvertWeightToOrignalForm } = require("../../Functions/WeightManipulation");


test('Reduce Measurement to its lowest form', () => {
  const Amount = 1000;
  const Unit = 'Kgs';
  expect(ConvertWeightToOrignalForm(Amount, Unit)).toEqual(1);
});

test('Measurement is not changed', () => {
  const Amount = 1000;
  const Unit = 'grams';
  expect(ConvertWeightToOrignalForm(Amount, Unit)).toEqual(1000);
});

test('Measurement is not changed with cups', () => {
  const Amount = 10;
  const Unit = 'cups';
  expect(ConvertWeightToOrignalForm(Amount, Unit)).toEqual(10);
});

test('Return Measurement to its Orginal form', () => {
  const Amount = 1;
  const Unit = 'Kgs';
  expect(ConvertWeightToLowestForm(Amount, Unit)).toEqual(1000);
});

test('Return Measurement with no changes', () => {
  const Amount = 100;
  const Unit = 'grams';
  expect(ConvertWeightToLowestForm(Amount, Unit)).toEqual(100);
});

test('Return Measurement with no changes', () => {
  const Amount = 100;
  const Unit = 'cups';
  expect(ConvertWeightToLowestForm(Amount, Unit)).toEqual(100);
});
