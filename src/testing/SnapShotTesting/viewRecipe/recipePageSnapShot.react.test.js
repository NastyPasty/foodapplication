
import React from 'react';
import { Provider } from 'react-redux';
import RecipePage from '../../../components/viewRecipe/recipePage'
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import { MemoryRouter } from 'react-router-dom'
import { MockedProvider } from '@apollo/react-testing';



const mockStore = configureStore([]);
describe('KitchenEquipmentModal Renders correctly', () => {
    let store;
    let component;
   
    beforeEach(() => {
      store = mockStore({
        myState: 'sample text',
            recipePage:{
                recipeName: 'test',
                recipeServings: 4,
                recipeImage: ''
            }
      });
   
      component = renderer.create(
        <MockedProvider >
        <MemoryRouter initialEntries={[ {pathname: '/home'}]}>
       
        <Provider store={store}>
      
          <RecipePage />

        </Provider>
        </MemoryRouter>
        </MockedProvider>
      );
    });


it('renders correctly', () => {

  expect(component.toJSON()).toMatchSnapshot();
});
  });
