import React from 'react';
import { Provider } from 'react-redux';
import WeightDropDown from '../../../components/DropDown/WeightDropDown'
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
 
const mockStore = configureStore([]);
describe('My Connected React-Redux Component', () => {
    let store;
    let component;
   
    beforeEach(() => {
      store = mockStore({
        myState: 'sample text',
      });
   
      component = renderer.create(
        <Provider store={store}>
          <WeightDropDown />
        </Provider>
      );
    });


it('renders correctly', () => {

  expect(component.toJSON()).toMatchSnapshot();
});
  });
