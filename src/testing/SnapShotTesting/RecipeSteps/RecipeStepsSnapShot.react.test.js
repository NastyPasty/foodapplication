
import React from 'react';
import { Provider } from 'react-redux';
import RecipeSteps from '../../../components/RecipeSteps/RecipeSteps'
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import { MemoryRouter } from 'react-router-dom'
import { MockedProvider } from '@apollo/react-testing';



const mockStore = configureStore([]);
describe('KitchenEquipmentModal Renders correctly', () => {
    let store;
    let component;
   
    beforeEach(() => {
      store = mockStore({
        myState: 'sample text',
     
      });
   
      component = renderer.create(
        <MockedProvider >
       
        <Provider store={store}>
      
          <RecipeSteps />

        </Provider>
        </MockedProvider>
      );
    });


it('renders correctly', () => {

  expect(component.toJSON()).toMatchSnapshot();
});
  });
