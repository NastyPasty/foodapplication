
import React from 'react';
import { Provider } from 'react-redux';
import AppBar from '../../../components/AppBar/appBar'
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import { MemoryRouter } from 'react-router-dom'
import { MockedProvider } from '@apollo/react-testing';




const mockStore = configureStore([]);
describe('AppBar Renders correctly', () => {
    let store;
    let component;
   
    beforeEach(() => {
      store = mockStore({
        myState: 'sample text',
        unit:{
            unit: 'Grams'
        }
      });
   
      component = renderer.create(
        <MockedProvider >
        <MemoryRouter initialEntries={[ {pathname: '/login'}]}>
        <Provider store={store}>
      
          <AppBar />

        </Provider>
        </MemoryRouter>
        </MockedProvider>
      );
    });


it('renders correctly', () => {

  expect(component.toJSON()).toMatchSnapshot();
});
  });
