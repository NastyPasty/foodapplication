import React from 'react';
import LoginButton from '../../../components/AppBar/LoginButton'
import renderer  from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom'
it('renders correctly', () => {
  const tree = renderer
    .create(
    <MemoryRouter initialEntries={[ {pathname: '/login'}]}>
         <LoginButton/>
    </MemoryRouter>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
