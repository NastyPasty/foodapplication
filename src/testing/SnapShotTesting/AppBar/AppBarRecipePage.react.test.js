import React from 'react';
import AppBar from '../../../components/AppBar/appBarRecipePage'
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';

it('renders correctly', () => {
  const tree = renderer
    .create(
    <MemoryRouter initialEntries={['/home']}>
         <AppBar/>
    </MemoryRouter>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
