
import React from 'react';
import { Provider } from 'react-redux';
import {MyModal} from '../../../components/KitchenEquipment/addToKitchenEquipmentModal'
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import { MemoryRouter } from 'react-router-dom'
import { MockedProvider } from '@apollo/react-testing';



const mockStore = configureStore([]);
describe('KitchenEquipmentModal Renders correctly', () => {
    let store;
    let component;
   
    beforeEach(() => {
      store = mockStore({
        myState: 'sample text',
     
      });
   
      component = renderer.create(
        <MockedProvider >
        <MemoryRouter initialEntries={[ {pathname: '/login'}]}>
        <Provider store={store}>
      
          <MyModal />

        </Provider>
        </MemoryRouter>
        </MockedProvider>
      );
    });


it('renders correctly', () => {

  expect(component.toJSON()).toMatchSnapshot();
});
  });
