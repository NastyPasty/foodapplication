
import React from 'react';
import { Provider } from 'react-redux';
import KitchenEquipmentButton from '../../../components/KitchenEquipment/addToKitchenEquipmentButton'
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import { MemoryRouter } from 'react-router-dom'
import { MockedProvider } from '@apollo/react-testing';
import { InMemoryCache } from 'apollo-cache-inmemory'



const mockStore = configureStore([]);
describe('AppBar Renders correctly', () => {
    let store;
    let component;
   
    beforeEach(() => {
      store = mockStore({
        myState: 'sample text',
     
      });
   
      component = renderer.create(
        <MockedProvider >
        <MemoryRouter initialEntries={[ {pathname: '/login'}]}>
        <Provider store={store}>
      
          <KitchenEquipmentButton />

        </Provider>
        </MemoryRouter>
        </MockedProvider>
      );
    });


it('renders correctly', () => {

  expect(component.toJSON()).toMatchSnapshot();
});
  });
