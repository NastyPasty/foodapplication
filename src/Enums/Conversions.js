import Units_ from './UnitsEnums'
/**
 * Map of units to their next up/down conversions.
 * Sets breakpoints at which to convert into the other unit.
 * @private {!Object}
 */
const CONVERSIONS_ = {
  cup: {
    to: {
      tablespoon: 16
    },
    min: {
      value: 1 / 4,
      next: Units_.TABLESPOON
    }
  },
  tablespoon: {
    to: {
      teaspoon: 3,
      cup: 1 / 16
    },
    min: {
      value: 1,
      next: Units_.TEASPOON
    },
    max: {
      value: 4,
      next: Units_.CUP
    }
  },
  teaspoon: {
    to: {
      tablespoon: 1 / 3
    },
    max: {
      value: 3,
      next: Units_.TABLESPOON
    }
  },
  kg: {
    to: {
      gram: 1000
    },
    min: {
      value: 0.00,
      next: Units_.GRAM
    }
  },
  gram: {
    to: {
      kg: 1
    },
    min: {
      value: 0.00
    },
    max: {
      value: 1000,
      next: Units_.KG
    }
  }
};

export default CONVERSIONS_;