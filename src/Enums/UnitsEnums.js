
/**
 * Names of measurement units.
 * @private @enum {string}
 */
const Units_ = {
  TEASPOON: 'teaspoon',
  TABLESPOON: 'tablespoon',
  CUP: 'cup',
  GRAM: 'gram',
  KG: 'kg',
  ML: 'ml',
  Litre: 'litre',

};

export default Units_;