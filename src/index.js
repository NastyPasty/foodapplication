import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { Provider } from 'react-redux'
import store from './redux/Store'
import { AUTH_TOKEN } from './components/Accounts/constants'
import { InMemoryCache } from 'apollo-cache-inmemory'

const client = new ApolloClient(
  {
    uri: 'http://localhost:3000/dev/graphql',
    cache: new InMemoryCache()
  });


ReactDOM.render(
  <ApolloProvider client={client}>
    <Provider store={store}>
      <App />
    </Provider>
  </ApolloProvider>,
  document.getElementById('root')
);

serviceWorker.unregister();
