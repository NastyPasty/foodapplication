const validation = {
  validate: (variable) => {
    // true means invalid, so our conditions got reversed
    return {
      variable: variable.length === 0,
    };
  }
}

module.exports =
  validation.validate