function ConvertWeightToLowestForm(Amount, Unit) {
        if (Unit === 'Kgs' || Unit === 'Litres') {
                return WeightMultiple(Amount);
        }
        else {
                return Amount
        }
}

function ConvertWeightToOrignalForm(Amount, Unit) {
        if (Unit === 'Kgs' || Unit === 'Litres') {
                return WeightDivide(Amount);
        }
        else {
                return Amount
        }
}

function WeightDivide(Amount) {
        return Amount /= 1000
}

function WeightMultiple(Amount) {
        return Amount *= 1000
}


module.exports = {
        ConvertWeightToLowestForm,
        ConvertWeightToOrignalForm
};