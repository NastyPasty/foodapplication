import React, { useState } from 'react';
import styles from './App.module.css';
import RecipeList from "./components/RecipeFrontPageList/RecipeList";
import RecipePage from "./components/viewRecipe/recipePage";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import KitchenEquipmentList  from "./components/KitchenEquipment/viewKitchenEquipment";
import FoodItemsList from "./components/FoodItems/foodItemsList";
import Footer from "./components/Footer/footer";
import ButtonAppBar from "./components/AppBar/appBar";
import Login from "./components/Accounts/Login";
import Container from '@material-ui/core/Container';



const App = () => {

  return (

      <Router>

        <Switch>
          <Route exact path="/login" component={Login} >

          </Route>
          <Route exact path='/home/' RecipeList>
            <ButtonAppBar />
              <Container   className={styles.flexcontainer} >
              <KitchenEquipmentList className={styles.flexitem} />
              <FoodItemsList  className={styles.flexitem}  />
  
              </Container>
         

            <RecipeList className={styles.RecipeList} />
          </Route>
          <Route exact path='/view/:recipeid' component={RecipePage} />}

        </Switch>
        <div className={styles.phantomStyle}>
          <Footer className={styles.Footer} />
        </div>
      </Router>
  );
}

export default App;
