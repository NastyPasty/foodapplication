import React, { useState } from 'react'
import { AUTH_TOKEN } from './constants'
import Button from '@material-ui/core/Button';
import { useMutation } from 'react-apollo';
import LoginMutation from '../../graphql/mutations/Login/loginMutation';
import SignUpMutation from '../../graphql/mutations/Login/signUpMutation';
import AppBar from '../../components/AppBar/appBarRecipePage';

const Login = () => {

  const [loginBoolean, setLogin] = useState(true)
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");

  const [login] = useMutation(LoginMutation);
  const [signup] = useMutation(SignUpMutation);

  const loginUser = (email, password) => {

    login({
      variables: {
        email: email,
        password: password
      }
    }).then(
      data => _confirm(data.data.login.token)
    )
  }

  const signUpUser = (name, email, password) => {



    signup({
      variables: {
        name: name,
        email: email,
        password: password
      }
    }).then(
      data => _confirm(data.data.signup.token)
    )
  }

  const _confirm = async (token) => {

    _saveUserData(token)
  }

  const _saveUserData = token => {
    localStorage.setItem(AUTH_TOKEN, token)
  }

  return (

    <div>
      <AppBar />
      <h4 className="mv3">{loginBoolean ? 'Login' : 'Sign Up'}</h4>
      <div className="flex flex-column">
        {!loginBoolean && (
          <input
            value={name}
            onChange={e => setName(e.target.value)}
            type="text"
            placeholder="Your name"
          />
        )}
        <input
          value={email}
          onChange={e => setEmail(e.target.value)}
          type="text"
          placeholder="Your email address"
        />
        <input
          value={password}
          onChange={e => setPassword(e.target.value)}
          type="password"
          placeholder="Choose a safe password"
        />
      </div>
      <div className="flex mt3">


        <div>
          <Button onClick={() => loginBoolean ? loginUser(email, password) : signUpUser(name, email, password)}>
            {loginBoolean ? 'login' : 'create account'}

          </Button>
        </div>


        <Button className="pointer button" onClick={() => setLogin(!loginBoolean)}>
          {loginBoolean
            ? 'need to create an account?'
            : 'already have an account?'}
        </Button>
      </div>
    </div>
  )


}







export default Login