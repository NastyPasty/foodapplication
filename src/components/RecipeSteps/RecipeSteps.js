import React, { useState, useRef, useEffect } from "react";
import List from '@material-ui/core/List';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Mutation, useMutation, Query } from "react-apollo"
import AddRecipeStep from "../../graphql/mutations/addMutations/addRecipeMethodStep"
import { InputBase } from "@material-ui/core";
import RemoveRecipeStep from "../../graphql/mutations/removeMutations/removeRecipeStep"
import UpdateRecipeStep from "../../graphql/mutations/updateMutations/updateRecipeStep"
import styles from "../../App.module.css"
import ReactHeader from '../Headers/Header'
import GetSingleRecipeSteps from "../../graphql/queries/GetSingleRecipeSteps";


const RecipeList = (props) => {


  return (
    <List className="list-group">
      {props.items.map((item, index) => (
        <RecipeListItem
          stepsRefetch={props.stepsRefetch}
          recipeID={props.recipeID}
          recipeStepID={item.id}
          stepInformation={item.stepInformation}
          stepNumber={index + 1}
        />
      ))}
    </List>

  )
}





const RecipeListItem = ({
  recipeStepID,
  stepInformation,
  stepNumber,
  recipeID
}) => {
  const [editMode, setEditMode] = useState(false);
  const [inputValue, setInputValue] = useState(stepInformation);
  const [updateRecipeStep] = useMutation(UpdateRecipeStep)
  const [removeRecipeStep] = useMutation(RemoveRecipeStep)


  const handleEditValue = () => {
    setEditMode(false);
    updateRecipeStep({
      variables: {
        id: recipeStepID,
        stepInformation: inputValue
      }, refetchQueries: [{
        query: GetSingleRecipeSteps,
        variables: { recipeID: recipeID }
      }]
    })

  };
  const removeStep = () => {
    removeRecipeStep({
      variables: {
        id: recipeStepID
      }, refetchQueries: [{
        query: GetSingleRecipeSteps,
        variables: { recipeID: recipeID }
      }]
    })


  }

  const keyPress = e => {
    if (e.key === "Enter") {
      handleEditValue();
    }
  };

  return (

    <List className="list-group-item">
      <div>
        <InputBase
          style={{ width: 10, margin: 8 }}
          value={stepNumber}
          type="text"
          autoFocus
          disabled={true}
          inputProps={{ "aria-label": "naked" }}
        />
        <span onClick={() => setEditMode(true)}>

          {editMode ? (

            <TextField id="standard-basic"
              type="text"
              value={inputValue}
              onChange={e => setInputValue(e.target.value)}
              onBlur={handleEditValue}
              onKeyPress={keyPress}
              autoFocus
            />
          ) : (
              stepInformation
            )}
        </span>
        <Button
          onClick={removeStep}
          className="close">
          &times;
        </Button>
      </div>
    </List>

  )
}


const RecipeForm = ({ recipeID }) => {
  const inputEl = useRef(null);

  useEffect(() => {
    inputEl.current.focus();
  }, []);


  return (
    <Mutation mutation={AddRecipeStep} >
      {(addRecipeStep, { loading, error, refetchQueries }) => (
        <form
          onSubmit={e => {
            e.preventDefault();
            addRecipeStep({
              variables:
              {
                recipeID: recipeID,
                stepInformation: inputEl.current.value,
              },
              refetchQueries: [{
                query: GetSingleRecipeSteps,
                variables: { recipeID: recipeID }
              }]
            });

            recipeID = '';
            inputEl.current.value = '';
          }}>


          <TextField
            id="outlined-multiline-static"
            label="Step Information"
            inputRef={inputEl}
            multiline
            rows={4}
            fullWidth
            style={{ margin: 8 }}
            variant="outlined" />

          <Button variant='contained' type="submit" className="btn btn-light">
            Add
      </Button>

          {loading && <p>Loading...</p>}
          {error && <p>Error :( Please try again</p>}
        </form>

      )}
    </Mutation>
  )
};




const RecipeSteps = props => {

  let recipeID = props.recipeID;




  return (

    <Query query={GetSingleRecipeSteps}
      variables={{ recipeID: recipeID }}>
      {({ loading, error, data }) => {

        if (loading) return 'Loading...';
        if (error) return `Error! ${error.message}`;
        return (

          <div className={styles.RecipeStepList}>
            <ReactHeader headerValue={"Method"} />
            <RecipeList recipeID={recipeID} items={data.recipeSteps} />
            <RecipeForm recipeID={recipeID} />
          </div>

        )
      }}
    </Query>
  );
}

export default RecipeSteps;