import React, { useEffect, useState } from 'react';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import Button from '@material-ui/core/Button'
import { Query, Mutation } from 'react-apollo';
import GetAllRecipes from '../../graphql/queries/GetAllRecipes'
import { Link } from 'react-router-dom';
import RemoveRecipeName from "../../graphql/mutations/removeMutations/removeRecipeName"
import styles from "./RecipeList.module.css"
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem';
import { connect } from 'react-redux';
import { addRecipeName, addRecipeImage, addRecipeServing } from '../../redux/Actions';
import Container from '@material-ui/core/Container';
import useInfiniteScroll from'../../Functions/useInfiniteScroll';

const DeleteButton = ({ recipe }) => {
  return (
    <Mutation mutation={RemoveRecipeName} refetchQueries={[{ query: GetAllRecipes }]} >
      {(removeRecipeName, { loading, error }) => (
        <div>
          <form className={styles.deleteButton} onSubmit={e => {
            e.preventDefault();
            removeRecipeName({
              variables:
              {
                _id: recipe.id
              }
            });
          }}> <Button type="submit"
            className="close" color={'secondary'} >
              &times;</Button>
          </form>

          {loading && <p>Loading...</p>}
          {error && <p>Error :( Please try again</p>}
        </div>
      )}
    </Mutation>
  )
}

const RecipeList = (props) => {
  return (

    <Query query={GetAllRecipes}>
      {({ loading, error, data }) => {
        if (loading) return 'Loading...';
        if (error) return `Error! ${error.message}`;
        return (
          <div className={styles.root}>
            <Recipe addRecipeName={props.addRecipeName}
              addRecipeImage={props.addRecipeImage}
              addRecipeServing={props.addRecipeServing}
              items={data.recipeNames} />

          </div>);
      }}
    </Query>

  );
}



const Recipe = (props) => {
  const [filtered, setFiltered] = useState(props.items)
  const [select, setSelected] = useState("")
  const [isFetching, setIsFetching] = useInfiniteScroll(fetchMoreListItems);

  const handleChange = (e) => {
    // Variable to hold the original version of the list
    let currentList = [];
    // Variable to hold the filtered list before putting into state
    let newList = [];

    // If the search bar isn't empty
    if (e.target.value !== "") {

      // Assign the original list to currentList
      currentList = props.items;
      // Use .filter() to determine which items should be displayed
      // based on the search terms
      newList = currentList.filter(item => {
        let lc;
        let filter;
        // change current item to lowercase
        if (select == "recipeName") {
          lc = item.recipeName.toLowerCase();
          filter = e.target.value.toLowerCase();
          return lc.includes(filter);
        }
        else {
          lc = item.totalTime
          filter = parseInt(e.target.value);
          return lc == filter;
        }
        // change search term to lowercase

        // check to see if the current list item includes the search term
        // If it does, it will be added to newList. Using lowercase eliminates
        // issues with capitalization in search terms and search content

      });
    } else {
      // If the search bar is empty, set newList to original task list
      newList = props.items;
    }
    // Set the filtered state based on what our rules added to newList
    setFiltered(newList)

  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, []);

  useEffect(() => {
    if (!isFetching) return;
    fetchMoreListItems();
  }, [isFetching]);

  function handleScroll() {
    if (window.innerHeight + document.documentElement.scrollTop !== document.documentElement.offsetHeight || isFetching) return;
    setIsFetching(true);
  }

  function fetchMoreListItems() {
    setTimeout(() => {
    //  setListItems(prevState => ([...prevState, ...Array.from(Array(20).keys(), n => n + prevState.length + 1)]));
      setIsFetching(false);
    }, 2000);
  }
  return (
    <Container>
      <div className={styles.Search}>
      <input onChange={e => handleChange(e)} />
      <Select  defaultValue={"recipeName"} 
      value={select} onChange={e => setSelected(e.target.value)}>

        <MenuItem value={"recipeName"}>Recipe Name</MenuItem>
        <MenuItem value={"TotalTime"}>Total Cook Time</MenuItem>

      </Select>

      </div>      <>
      <GridList classes={{ root: styles.root }}  cols={'inline-block'}>
        {filtered.map((recipe) => (
          <div >
            <Link style={{ textDecoration: 'none' }} onClick={() => {
              props.addRecipeName(recipe.recipeName),
              props.addRecipeServing(recipe.servings),
              props.addRecipeImage(recipe.file)
            }} to={{
              pathname: '/view/' + recipe.recipeName,
            }}>
              <GridListTile cols={recipe.cols || 1} >

                <img className={styles.ImageSize} alt={recipe.recipeName} src={recipe.file} />

                <GridListTileBar title={recipe.recipeName} subtitle={"Total Time: " + recipe.totalTime}
                actionIcon={
                  <DeleteButton recipe={recipe}/>
                }/>
              </GridListTile>
            </Link>
           
          </div>

        ))}
      </GridList>
      {isFetching && 'Fetching more list items...'}
      </>
    </Container>
  );
}


const mapDispatchToProps = (dispatch) => {
  return {
    addRecipeName: recipeName => dispatch(addRecipeName(recipeName)),
    addRecipeImage: recipeImage => dispatch(addRecipeImage(recipeImage)),
    addRecipeServing: recipeServings => dispatch(addRecipeServing(recipeServings))
  }
}

const RecipeGrid = connect(null,
  mapDispatchToProps)(RecipeList)


export default RecipeGrid;