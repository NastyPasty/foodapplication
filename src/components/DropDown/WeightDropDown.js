import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { addUnit } from "../../redux/Actions";
import { connect } from "react-redux";
import { props } from 'bluebird';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 220,
    paddingTop: 10
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

function MeasurementDropDown(props) {
  const classes = useStyles();
  const [state, setState] = React.useState({
    measurement: '',
    name: 'hai',
  });

  const handleChange = (event) => {
    const name = event.target.name;
    props.addUnit(event.target.value);
    setState({
      ...state,
      [name]: event.target.value,
    });
  };

  return (
    <div>
      <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel htmlFor="outlined-age-native-simple">Measurement</InputLabel>
        <Select
          native
          value={state.measurement}
          onChange={handleChange}
          label="Measurement Unit"
          inputProps={{
            name: 'measurement',
            id: 'outlined-age-native-simple',
          }}
        >
          <option aria-label="None" value="" />
          <option value={'TeaSpoon'}>TeaSpoon</option>
          <option value={'TableSpoon'}>TableSpoon</option>
          <option value={'Cup'}>Cup</option>
          <option value={'Grams'}>Grams</option>
          <option value={'Kgs'}>Kgs</option>
          <option value={'Mls'}>Mls</option>
          <option value={'Litres'}>Litres</option>
          <option value={'Small'}>Small</option>
          <option value={'Medium'}>Medium</option>
          <option value={'Large'}>Large</option>
        </Select>
      </FormControl>

    </div>
  );
}


const mapDispatchToProps = (dispatch) => {
  return {
    addUnit: unit => dispatch(addUnit(unit))
  }
}

const WeightDropDown = connect(null,
  mapDispatchToProps)(MeasurementDropDown)


export default WeightDropDown