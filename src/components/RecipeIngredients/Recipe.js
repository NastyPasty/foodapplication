import React, { Component } from "react";
import TextField from '@material-ui/core/TextField';
import { connect } from 'react-redux';
import { useQuery } from 'react-apollo'
import { addservingIngredient, resetServingIngredient } from "../../redux/Actions"
import GetSingleRecipeFoodItems from "../../graphql/queries/GetSingleRecipesFoodItems"
var Quantity = require('./quantity.js');
var Measurement = require('./measurement.js');


class Recipe extends Component {
  constructor(props) {
    super(props)
    this.onChange = this.onChange.bind(this);
    this.getStep = this.getStep.bind(this);
    this.state = {
      serves: props.servings,
      scale: 1,
      defaultServers: props.servings
    }
  }
  /**
   * Calculate a natural amount to step each change in serving size.
   * Equivalent to the largest factor <= the square root of the serving size.
   */
  getStep(serves) {
    let sqrt = Math.floor(Math.sqrt(serves));
    while (serves % sqrt) {
      sqrt--;
    }
    return sqrt;
  }

  getInitialState() {
    return {
      serves: this.state.recipeServings,
      scale: 1
    };
  };

  onChange(e) {
    const serves = Math.floor(Math.abs(e.target.value));
    this.props.resetServingIngredient("clear")
    this.setState({
      serves: serves,
      scale: serves / this.state.defaultServers // change the 4 to the defined server when working
    });
  };

  render() {
    return (
      <section>
        <h2>
          Serves
          <TextField
            type="number" min="0" x
            inputProps={{ min: 0, style: { textAlign: 'center' } }}
            value={this.state.serves}
            step={this.getStep(this.state.serves)}
            onChange={this.onChange} />
        </h2>
        <Ingredients addservingIngredient={this.props.addservingIngredient} ingredients={this.props.ingredients} scale={this.state.scale} />
      </section>

    )
  }
};

const Ingredients = (props) => {

  return (
    <ul>
      {props.ingredients.map((ingredient, i) =>
        <Ingredient addservingIngredient={props.addservingIngredient} key={i} scale={props.scale} {...ingredient} />
      )}
    </ul>
  );
};

const FoodItemsDefaultWeight = (ingredientName) => {

  const { data, loading, error } = useQuery(GetSingleRecipeFoodItems, { variables: { nameOfItem: ingredientName } });
  if (loading) return 'Loading...';
  if (error) return 0;

  return data.singleRecipeFoodItems.weightOfItem ? data.singleRecipeFoodItems.weightOfItem : 0;
};

const Ingredient = (props) => {

  let DefaultWeight = FoodItemsDefaultWeight(props.ingredientName);

  const quantity = new Quantity(props.ingredientQuantity).multiply(props.scale, props.ingredientUnit);
  const unit = props.ingredientUnit;
  const measurement = new Measurement(quantity, unit).convertUnits();
  if (DefaultWeight != 'Loading...')
    props.addservingIngredient({
      quantity: quantity.toString(),
      unit: unit,
      ingredientName: props.ingredientName,
      foodItemsDefaultWeight: DefaultWeight
    });

  return (
    <li>{measurement.toString()} {props.ingredientName}</li>
  );

};

function mapStateToProps(state) {
  return {
    recipeServings: state.recipeServings
  };
};
function mapDispatchToProps(dispatch) {
  return {
    addservingIngredient: servingIngredients => dispatch(addservingIngredient(servingIngredients)),
    resetServingIngredient: clear => dispatch(resetServingIngredient(clear))
  }
}

const recipe = connect(mapStateToProps,
  mapDispatchToProps
)(Recipe);


export default recipe;