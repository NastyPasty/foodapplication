import React, { useState, useRef, useEffect, Component } from "react";
import List from '@material-ui/core/List';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Mutation, useMutation, Query } from "react-apollo"
import AddIngredients from "../../graphql/mutations/addMutations/addIngredients"
import RemoveIngredients from "../../graphql/mutations/removeMutations/removeIngredients"
import UpdateIngredients from "../../graphql/mutations/updateMutations/updateIngredients"
import styles from "../../App.module.css";
import Recipe from "./Recipe";
import ReactHeader from '../Headers/Header';
import GetSingleRecipeIngredients from '../../graphql/queries/GetSingleRecipeIngredients'
import MeasurementDropDown from '../DropDown/WeightDropDown';
import { connect } from "react-redux";

const RecipeList = (props) => {

  return (
    <List className="list-group">
      {props.recipeIngredients.map(item => (
        <RecipeListItem
          ingredientName={item.ingredientName}
          ingredientQuantity={item.ingredientQuantity}
          ingredientUnit={item.ingredientUnit}
          id={item.id}
          recipeID={props.recipeID} />
      ))}
    </List>

  )
}

const RecipeListItem = ({
  ingredientName,
  ingredientQuantity,
  ingredientUnit,
  id,
  recipeID }) => {
  const [editMode, setEditMode] = useState(false);
  const [inputIngredientName, setInputIngredientName] = useState(ingredientName);
  const [inputQuantity, setInputQuantity] = useState(ingredientQuantity);
  const [inputUnit, setInputUnit] = useState(ingredientUnit);
  const [updateIngredient] = useMutation(UpdateIngredients)
  const [removeIngredients] = useMutation(RemoveIngredients)

  const handleEditValue = () => {

    setEditMode(false);
    updateIngredient({
      variables: {
        _id: id,
        ingredientName: inputIngredientName,
        ingredientQuantity: inputQuantity,
        ingredientUnit: inputUnit
      }, refetchQueries: [{
        query: GetSingleRecipeIngredients,
        variables: { recipeID: recipeID }
      }]
    })
  };

  const removeIngredient = () => {
    removeIngredients({
      variables: {
        _id: id
      }, refetchQueries: [{
        query: GetSingleRecipeIngredients,
        variables: { recipeID: recipeID }
      }]
    })
  }

  const keyPress = e => {
    if (e.key === "Enter") {
      handleEditValue();
    }
  };

  return (

    <List className="list-group-item">

      <div >

        <TextField id="standard-basic"
          onClick={() => setEditMode(true)}
          type="text"
          style={{ width: 100, margin: 8 }}
          value={inputQuantity}
          onChange={e => setInputQuantity(e.target.value)}
          onBlur={handleEditValue}
          editMode={false}
          onKeyPress={keyPress} />

        <TextField id="standard-basic"
          onClick={() => setEditMode(true)}
          type="text"
          style={{ width: 100, margin: 8 }}
          value={inputUnit}
          onChange={e => setInputUnit(e.target.value)}
          onBlur={handleEditValue}
          editMode={false}
          onKeyPress={keyPress} />

        <TextField id="standard-basic"
          onClick={() => setEditMode(true)}
          style={{ width: 100, margin: 8 }}
          type="text"
          value={inputIngredientName}
          onChange={e => setInputIngredientName(e.target.value)}
          onBlur={handleEditValue}
          onKeyPress={keyPress}
        />

        <Button onClick={removeIngredient} className="close" >
          &times;
        </Button>
      </div>
    </List>

  )
}

const RecipeForm = (props) => {
  const inputEl = useRef(null);
  const inputELQuantity = useRef(null);

  useEffect(() => {
    inputEl.current.focus();
    inputELQuantity.current.focus();
  }, []);



  return (

    <Mutation mutation={AddIngredients} >
      {(addIngredients, { loading, error }) => (
        <form
          onSubmit={e => {
            e.preventDefault();
            addIngredients({
              variables:
              {
                recipeID: props.recipeName,
                ingredientQuantity: inputELQuantity.current.value,
                ingredientUnit: props.unitOfMeasurement,
                ingredientName: inputEl.current.value,
              }, refetchQueries: [{
                query: GetSingleRecipeIngredients,
                variables: { recipeID: props.recipeName }
              }]
            });


            inputELQuantity.current.value = '';
            inputEl.current.value = '';
          }}>

          <TextField
            id="outlined-multiline-static"
            label="Ingredient Quantity"
            inputRef={inputELQuantity}
            rows={6}
            style={{ margin: 10 }}
            variant="outlined" />

          <MeasurementDropDown />

          <TextField
            id="outlined-multiline-static"
            label="Ingredient Name"
            inputRef={inputEl}
            rows={6}
            style={{ margin: 10 }}
            variant="outlined" />

          <Button variant='contained' type="submit" className="btn btn-light">
            Add
        </Button>
          {loading && <p>Loading...</p>}
          {error && <p>Error :( Please try again</p>}
        </form>)}
    </Mutation>
  )
};


const RecipeSteps = props => {

  let recipeName = props.recipeName;


  return (
    <Query query={GetSingleRecipeIngredients}
      variables={{ recipeID: recipeName }}>
      {({ loading, error, data }) => {

        if (loading) return 'Loading...';
        if (error) return `Error! ${error.message}`;
        return (


          <div className={styles.RecipeIngredientsList}>
            <ReactHeader headerValue={"Ingredients"} />
            <Recipe servings={props.servings} ingredients={data.recipeIngredients} />
            <RecipeList recipeIngredients={data.recipeIngredients} recipeID={recipeName} />
            <RecipeForm unitOfMeasurement={props.unitOfMeasurement} recipeName={recipeName} />


          </div>
        )
      }}
    </Query>
  );
}

const mapStateToProps = state => {
  return {
    unitOfMeasurement: state.unit.unit,

  };
};

export default connect(mapStateToProps)(RecipeSteps);
