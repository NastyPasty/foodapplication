import React, { useState, useEffect } from "react"
import RecipeSteps from '../RecipeSteps/RecipeSteps'
import RecipeIngredients from '../RecipeIngredients/RecipeIngredientsList'
import styles from '../../App.module.css'
import CompletedRecipeButton, { servingsData } from '../CompletedRecipe/completedRecipeButton'
import AppBarRecipe from "../AppBar/appBarRecipePage"
import { setRecipeName } from '../../redux/Actions'
import { connect } from "react-redux";

const RecipeIngredientsList = (props) => {


  return (

    <RecipeIngredients servings={props.servings} recipeName={props.recipeID} />

  );
}



const RecipeStepsList = (props) => {


  return (


    <RecipeSteps recipeID={props.recipeID} />
  );
}


const RecipePage = (props) => {



  return (

    <div >
      <AppBarRecipe />

      <div className={styles.RecipePage}>
        <img className={styles.RecipePageImage} src={props.recipeImage} />
        <RecipeIngredientsList servings={props.recipeServings} recipeID={props.recipeid} />
        <RecipeStepsList recipeID={props.recipeid} />
        <div className={styles.RecipePageCompletedButton}>
          <CompletedRecipeButton />
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    recipeid: state.recipePage.recipeName,
    recipeServings: state.recipePage.recipeServings,
    recipeImage: state.recipePage.recipeImage
  };
};

export default connect(mapStateToProps)(RecipePage);