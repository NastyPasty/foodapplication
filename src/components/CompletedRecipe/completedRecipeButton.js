import React from 'react'
import Button from '@material-ui/core/Button'
import GetAllFoodItems from '../../graphql/queries/GetAllFoodItems';
import { useMutation, useQuery } from 'react-apollo';
import { connect } from 'react-redux';
import CompletedFoodItem from '../../graphql/mutations/updateMutations/completedIngredients';
import { Link } from 'react-router-dom';

const GetFoodItemList = () => {
  const { loading, error, data } = useQuery(GetAllFoodItems);
  if (loading) {
    return <div>Loading...</div>; x
  }
  if (error) {
    console.error(error);
    return <div>Error!</div>;
  }
  return data.foodItems;
};

const mapStateToProps = state => {
  return { servingsData: state.servingIngredients };
};



const CompletedRecipeButton = props => {

  const foodItems = GetFoodItemList();
  const [completedFoodItem] = useMutation(CompletedFoodItem);
  const servingsData = props.servingsData.servingIngredients;


  const completeRecipe = () => {
    servingsData.map((item, index) => (
      completedFoodItem({
        variables: {
          nameOfItem: item.ingredientName,
          weightOfItem: parseInt(item.quantity),
          orignalWeight: parseInt(item.foodItemsDefaultWeight)
        }
      })))
  }
  return (
    <div>

      <Link to={"/home/"} style={{ textDecoration: 'none' }}  >
        <Button onClick={() => completeRecipe()}
          variant='contained'>Recipe Completed</Button>
      </Link>
    </div>
  )
}
const RecipeFinishedButton = connect(mapStateToProps)(CompletedRecipeButton)

export default RecipeFinishedButton;