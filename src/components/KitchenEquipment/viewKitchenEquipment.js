import React, {useState} from 'react';
import { useQuery, useMutation } from 'react-apollo';
import GetAllKitchenEquipment from '../../graphql/queries/GetAllKitchenEquipment'
import RemoveKitchenEquipment from "../../graphql/mutations/removeMutations/removeKitchenEquipment"
import styles from '../../App.module.css';
import MaterialTable from 'material-table';
import tableIcons from '../Tables/Tables';


export default function SimpleList() {
  const [removeKitchenEquipment] = useMutation(RemoveKitchenEquipment);


  

  const [state, setState] = useState({
    columns: [
      { title: 'Kitchen Equipment', field: 'kitchenEquipmentName' },
     
    ]
  });

  const ALLKitchenEquipment = () => {
    const { data, loading, error } = useQuery(GetAllKitchenEquipment);
    if (loading) return 'Loading...';
    if (error) return 0;
    return   data;
  }

  let kitchenEquipment = ALLKitchenEquipment();

 
  return (



    <div className={styles.KitchenEquipmentList}>

      <MaterialTable
        title='Kitchen Equipment'
        icons={tableIcons}
        columns={state.columns}
        data={kitchenEquipment.kitchenEquipments}
        
        editable={{
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                if (oldData) {
                  removeKitchenEquipment({
                    variables: { _id: oldData.id }
                    , refetchQueries: [{ query: GetAllKitchenEquipment }]
                  });

                }
              }, 600);
            }),
        }} />

    </div>
  )

}