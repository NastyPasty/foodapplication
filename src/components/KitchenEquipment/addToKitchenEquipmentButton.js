import React, { useState } from 'react';
import { MyModal } from './addToKitchenEquipmentModal';
import Button from '@material-ui/core/Button';
import styles from '../../App.module.css'
import  KitchenEquipmentList  from './viewKitchenEquipment'

const MODAL_A = 'modal_a';

const KitchenEquipmentButton = () => {


  const [currentModal, setCurrentModal] = useState(null)

  const toggleModal = Modal => event => {
    event.preventDefault();

    if (currentModal) {
      handleModalCloseRequest();
      return;
    }
    setCurrentModal(Modal);
  }

  const handleModalCloseRequest = () => {
    setCurrentModal(null);
    KitchenEquipmentList();
  }






  return (
    <div className={styles.btn}>

      <Button variant='contained'
        onClick={toggleModal(MODAL_A)}>Add New Kitchen Equipment</Button>

      <MyModal
        isOpen={currentModal === MODAL_A}
        onRequestClose={handleModalCloseRequest}
        askToClose={toggleModal(MODAL_A)}
      />
    </div>
  );
}

export default KitchenEquipmentButton;
