import React, { useState } from 'react';
import Modal from 'react-modal';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Mutation } from "react-apollo";
import GetAllKitchenEquipment from '../../graphql/queries/GetAllKitchenEquipment'
import AddKitchenEquipment from "../../graphql/mutations/addMutations/addKitchenEquipment"
const validate = require('../../Functions/validation');

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    zIndex: 0,
    transform: 'translate(-50%, -50%)'
  },

};


const MyModal = (props) => {

  const [kitchenEquipmentName, setKitchenEquipmentName] = useState("");


  const errors = validate(kitchenEquipmentName);
  const isDisabled = Object.keys(errors).some(x => errors[x]);

  const shouldMarkError = field => {
    const hasError = errors[field];

    return hasError;
  };
  return (
    <Mutation mutation={AddKitchenEquipment} 
    refetchQueries={[{ query: GetAllKitchenEquipment }]} 
    onCompleted={props.onRequestClose}>
      {(addKitchenEquipment, { loading, error }) => (
        <Modal
          closeTimeoutMS={150}
          isOpen={props.isOpen}
          onRequestClose={props.onRequestClose}
          style={customStyles}
        >
          <h2>Enter your Equipment you have in your Kitchen.</h2>

          <form onSubmit={e => {
            e.preventDefault();
            addKitchenEquipment({
              variables:
              {
                kitchenEquipmentName: kitchenEquipmentName
              }
            });

          }}>

            <p>kitchen Equipment Name</p>
            <TextField variant='outlined' name='receipeName'
              className={shouldMarkError("kitchenEquipmentName") ? "error" : ""}
              value={kitchenEquipmentName} onChange={e => setKitchenEquipmentName(e.target.value)}
              placeholder="Name of Equipment" />


            <div class='buttonStyle'>

              <Button type='submit' disabled={isDisabled} variant="contained" color='primary' >Send</Button>

              <Button variant="contained" onClick={props.onRequestClose} color='primary'>Close</Button>

            </div>
          </form>
          {loading && <p>Loading...</p>}
          {error && <p>Error :( Please try again</p>}
        </Modal>
      )}
    </Mutation>
  );
}
export {
  MyModal,
  validate
};
