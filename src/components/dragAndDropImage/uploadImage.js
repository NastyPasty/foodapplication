import React, { useCallback, useState } from "react";
import { DndProvider } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";
import TouchBackend from "react-dnd-touch-backend";
import update from "immutability-helper";
import cuid from "cuid";
import ReactHeader from '../Headers/Header';
import Dropzone from "./dropZone";
import ImageList from "./imageList";
import { isTouchDevice } from "./utils";

import "./uploadImage.css";

const backendForDND = isTouchDevice() ? TouchBackend : HTML5Backend;




function UploadImage() {
  const [images, setImages] = useState([]);


  const onDrop = useCallback(acceptedFiles => {
    acceptedFiles.map(file => {
      const reader = new FileReader();
      reader.onload = function (e) {
        setImages(prevState => [
          ...prevState,
          { id: cuid(), src: e.target.result }
        ]);
      };
      reader.readAsDataURL(file);
      return file;
    });
  }, []);

  const moveImage = (dragIndex, hoverIndex) => {
    const draggedImage = images[dragIndex];
    setImages(
      update(images, {
        $splice: [[dragIndex, 1], [hoverIndex, 0, draggedImage]]
      })
    );
  };

  return (

    <main className="App">
      <ReactHeader headerValue={"Upload Image of dish"} className="text-center" />
      <Dropzone onDrop={onDrop} accept={"image/*"} />
      <DndProvider backend={backendForDND}>
        <ImageList images={images} />
      </DndProvider>

    </main>
  );
}

export default UploadImage;