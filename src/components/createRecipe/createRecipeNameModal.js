import React, { useCallback, useState } from 'react';
import Modal from 'react-modal';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Mutation } from "react-apollo"
import AddRecipeName from '../../graphql/mutations/addMutations/AddRecipeName';
import { connect, useSelector, useDispatch } from 'react-redux'
import { DndProvider } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";
import TouchBackend from "react-dnd-touch-backend";
import cuid from "cuid";
import Dropzone from "../dragAndDropImage/dropZone";
import ImageList from "../dragAndDropImage/imageList";
import { isTouchDevice } from "../dragAndDropImage/utils";
import "../dragAndDropImage/uploadImage.css";
import ReactHeader from '../Headers/Header';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import { addRecipeName, addRecipeImage, addRecipeServing } from '../../redux/Actions';


const validate = require('../../Functions/validation');

const backendForDND = isTouchDevice() ? TouchBackend : HTML5Backend;

const customStyles = {

  content: {
    display: "inline-block",
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    zIndex: -1,
    transform: 'translate(-50%, -50%)',

  },

};

let imagesave = '';

function UploadImage() {
  const [images, setImages] = useState([]);


  const onDrop = useCallback(acceptedFiles => {
    acceptedFiles.map(file => {
      const reader = new FileReader();
      reader.onload = function (e) {
        setImages(prevState => [
          ...prevState,
          { id: cuid(), src: e.target.result },

        ]);

      };
      reader.readAsDataURL(file);
      return file;
    }

    );

  }, []);
  if (images.length !== 0) {
    images.map((image) => (
      imagesave = image.src)
    )
  }

  return (

    <main className="App">
      <ReactHeader headerValue={"Upload Image of dish"} className="text-center" />
      <Dropzone onDrop={onDrop} accept={"image/*"} />
      <DndProvider backend={backendForDND}>
        <ImageList images={images} />
      </DndProvider>

    </main>
  );
}


const MyModal = (props) => {

  const [recipeid, setRecipeid] = useState("");
  const [recipeName, setRecipeName] = useState("");
  const [timeToCook, setTimeToCook] = useState(0);
  const [prepTime, setPrepTime] = useState(0);
  const [totalTime, setTotalTime] = useState(0);
  const [servings, setServings] = useState(0);
  const [recipeTag, setRecipeTag] = useState("");






  const errors = validate(recipeName);
  const isDisabled = Object.keys(errors).some(x => errors[x]);

  const shouldMarkError = field => {
    const hasError = errors[field];


    return hasError;
  }

  return (

    <Mutation mutation={AddRecipeName}
      onCompleted={props.addRecipeName(recipeName),
        props.addRecipeServing(servings),
        props.addRecipeImage(imagesave), () => props.history.push("/view/" + recipeName)}>
      {(addRecipeName, { loading, error }) => (
        <Modal
          closeTimeoutMS={150}
          isOpen={props.isOpen}
          onAfterOpen={props.onAfterOpen}

          style={customStyles}
        >
          <h2>Enter your recipe Name.</h2>

          <form className={customStyles}
            onSubmit={e => {
              e.preventDefault();
              addRecipeName({
                variables:
                {
                  recipeid: recipeName,
                  recipeName: recipeName,
                  timeToCook: parseInt(timeToCook),
                  prepTime: parseInt(prepTime),
                  totalTime: parseInt(totalTime),
                  servings: parseInt(servings),
                  recipeTag: recipeTag,
                  file: imagesave
                }
              })

            }}>
            <UploadImage />

            <p>Recipe Name</p>
            <TextField variant='outlined' name='receipeName'
              className={shouldMarkError("recipeName") ? "error" : ""}
              value={recipeName} onChange={e => setRecipeName(e.target.value)}
              placeholder="Name of Recipe" />




            <p>Preperation Time</p>
            <TextField variant='outlined' name="prepTime"
              className={shouldMarkError("prepTime") ? "error" : ""}
              value={setPrepTime(prepTime)} onChange={e => setPrepTime(e.target.value)}
              placeholder="Time to prepare for cooking" />

            <p>Total Cooking Timen</p>
            <TextField variant='outlined' name='totalTime'
              className={shouldMarkError("totalTime") ? "error" : ""}
              value={totalTime} onChange={e => setTotalTime(e.target.value)}
              placeholder="Total time of recipe." />

            <p>Serving</p>
            <TextField id="filled-textarea" className={shouldMarkError("servings") ? "error" : ""}
              variant="filled" name="servings" value={servings}
              onChange={e => setServings(e.target.value)} placeholder="How many people will it feed."
            />

            <p>Recipe Tags</p>

            <TextField className={shouldMarkError("timeToCook") ? "error" : ""}
              variant="filled" name="timeToCook"
              value={recipeTag} onChange={e => setRecipeTag(e.target.value)}
              placeholder="Time to Cook." />




            <div className={customStyles} class='buttonStyle'>

              <Button type='submit' disabled={isDisabled} variant="contained" color='primary' >Send</Button>

              <Button variant="contained" onClick={props.onRequestClose} color='primary'>Close</Button>

            </div>
          </form>
          {loading && <p>Loading...</p>}
          {error && <p>Error :( Please try again</p>}
        </Modal>
      )}
    </Mutation>

  );

}



const mapDispatchToProps = (dispatch) => {
  return {
    addRecipeName: recipeName => dispatch(addRecipeName(recipeName)),
    addRecipeImage: recipeImage => dispatch(addRecipeImage(recipeImage)),
    addRecipeServing: recipeServings => dispatch(addRecipeServing(recipeServings))
  }
}


const RecipeNamemodal = connect(null,
  mapDispatchToProps)(MyModal)


export default withRouter(RecipeNamemodal);
