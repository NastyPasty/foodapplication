import React, { useState } from 'react';
import { Query, useMutation, useQuery } from 'react-apollo';
import GetAllFoodItems from '../../graphql/queries/GetAllFoodItems';
import styles from '../../App.module.css';
import UpdateFoodItem from '../../graphql/mutations/updateMutations/updateFoodItem';
import MaterialTable from 'material-table';
import RemoveFoodItem from '../../graphql/mutations/removeMutations/removeFoodItem';
import tableIcons from '../Tables/Tables';
const { ConvertWeightToOrignalForm, ConvertWeightToLowestForm } = require("../../Functions/WeightManipulation");

export default function SimpleList() {
  const [updateFoodItem] = useMutation(UpdateFoodItem);
  const [removeFoodItem] = useMutation(RemoveFoodItem);



  const [state, setState] = useState({
    columns: [
      { title: 'Name', field: 'nameOfItem' },
      { title: 'Weight', field: 'weightOfItem', type: 'numeric' },
      { title: 'Measurement', field: 'unitOfMeasurement' },
      { title: 'Expiry Date', field: 'expirationDate', type: 'date' },
    ]
  });

  const ALLFoodItems = () => {
    const { data, loading, error } = useQuery(GetAllFoodItems);
    if (loading) return 'Loading...';
    if (error) return 0;
    return data;
  }


  let data = ALLFoodItems();

  if (data != 'Loading...') {
    data.foodItems.map((item, index) => (
      item.weightOfItem = ConvertWeightToOrignalForm(item.weightOfItem, item.unitOfMeasurement)
    ));
  }
  return (



    <div className={styles.FoodItemList}>

      <MaterialTable
   

        title="Pantry Items"
        icons={tableIcons}
        columns={state.columns}
        data={data.foodItems}
        editable={{
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                if (oldData) {
                  updateFoodItem({
                    variables:
                    {
                      _id: oldData.id,
                      nameOfItem: newData.nameOfItem,
                      weightOfItem: parseInt(ConvertWeightToLowestForm(newData.weightOfItem, newData.unitOfMeasurement)),
                      unitOfMeasurement: newData.unitOfMeasurement,
                      expirationDate: new Date(newData.expirationDate)
                    }, refetchQueries: [{ query: GetAllFoodItems }]
                  });

                };
              }, 600);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                if (oldData) {
                  removeFoodItem({
                    variables: { _id: oldData.id }
                    , refetchQueries: [{ query: GetAllFoodItems }]
                  });

                }
              }, 600);
            }),
        }} />

    </div>
  )

}
