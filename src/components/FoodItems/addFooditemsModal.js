import React, { useState } from 'react';
import Modal from 'react-modal';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Mutation } from "react-apollo"
import AddFoodItem from "../../graphql/mutations/addMutations/addFoodItems"
import GetAllFoodItems from '../../graphql/queries/GetAllFoodItems';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MeasurementDropDown from '../DropDown/WeightDropDown';
import { connect } from "react-redux";

const { ConvertWeightToLowestForm } = require("../../Functions/WeightManipulation");
const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    
  },

};

function validate(nameOfItem, expirationDate, weightOfItem, unitOfMeasurement) {
  // true means invalid, so our conditions got reversed
  return {
    nameOfItem: nameOfItem.length === 0,
    expirationDate: expirationDate.length === 0,
    weightOfItem: weightOfItem.length === 0
  };
}

const MyModal = (props) => {
  const [nameOfItem, setNameOfItem] = useState("");
  const [expirationDate, setExpirationDate] = useState(new Date('dd/MM/yyyy'));
  const [weightOfItem, setWeightOfItem] = useState("");
  const [unitOfMeasurement, setUnitOfMeasurement] = useState("");

  const errors = validate(nameOfItem, expirationDate, weightOfItem, unitOfMeasurement);
  const isDisabled = Object.keys(errors).some(x => errors[x]);

  const convertWeight = () => {
    setWeightOfItem(ConvertWeightToLowestForm(parseInt(weightOfItem),
      unitOfMeasurement));
  }
  const shouldMarkError = field => {
    setUnitOfMeasurement(props.unitOfMeasurement)

    const hasError = errors[field];
    return hasError;
  };

  return (
    <Mutation mutation={AddFoodItem} onCompleted={props.onRequestClose} refetchQueries={[{ query: GetAllFoodItems }]}>
      {(addFoodItem, { loading, error }) => (
        <Modal
          closeTimeoutMS={150}
          isOpen={props.isOpen}
          onAfterOpen={props.onAfterOpen}
          onRequestClose={props.onRequestClose}
          style={customStyles}
        >
          <h2>Enter Food you have in your Kitchen.</h2>

          <form onSubmit={e => {
            e.preventDefault();
            convertWeight();
            addFoodItem({
              variables:
              {
                nameOfItem: nameOfItem,
                expirationDate: expirationDate,
                weightOfItem: parseInt(weightOfItem),
                unitOfMeasurement: unitOfMeasurement
              }
            });
          }}>

            <p>Food Item Name</p>
            <TextField variant='outlined' name='nameOfItem'
              className={shouldMarkError("nameOfItem") ? "error" : ""}
              value={nameOfItem} onChange={e => setNameOfItem(e.target.value)}
              placeholder="Name of Food" />

            <p>ExpirationDate</p>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker

                margin="normal"
                id="date-picker-dialog"
                className={shouldMarkError("expirationDate") ? "error" : ""}
                format="dd/MM/yyyy"
                value={expirationDate}
                onChange={e => setExpirationDate(e)}
                placeholder="Expiration Date"
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
            </MuiPickersUtilsProvider>


            <p>Food Item Weight </p>
            <TextField variant='outlined' name='weightOfItem'
              className={shouldMarkError("weightOfItem") ? "error" : ""}
              value={weightOfItem} onChange={e => setWeightOfItem(e.target.value)}
              placeholder="Weight of Item" />

            <MeasurementDropDown />


            <div class='buttonStyle'>

              <Button type='submit' disabled={isDisabled} variant="contained" color='primary' >Send</Button>

              <Button variant="contained" onClick={props.onRequestClose} color='primary'>Close</Button>

            </div>
          </form>
          {loading && <p>Loading...</p>}
          {error && <p>Error :( Please try again</p>}
        </Modal>
      )}
    </Mutation>
  );
}

const mapStateToProps = state => {
  return {
    unitOfMeasurement: state.unit.unit,

  };
};

export default connect(mapStateToProps)(MyModal);