import React, { useState } from 'react';
import MyModal from './addFooditemsModal';
import Button from '@material-ui/core/Button';
import styles from '../../App.module.css';
const MODAL_A = 'modal_a';

const RequestButton = () => {

  const [currentModal, setCurrentModal] = useState(null)

  const toggleModal = Modal => event => {
    event.preventDefault();

    if (currentModal) {
      handleModalCloseRequest();
      return;
    }
    setCurrentModal(Modal);
  }

  const handleModalCloseRequest = () => {
    setCurrentModal(null)
  }






  return (
    <div className={styles.btn}>

      <Button variant='contained'
        onClick={toggleModal(MODAL_A)}>Add Food</Button>

      <MyModal
        isOpen={currentModal === MODAL_A}
        onRequestClose={handleModalCloseRequest}
        askToClose={toggleModal(MODAL_A)}
      />
    </div>
  );
}

export default RequestButton;
