import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import FoodItemsButton from "../FoodItems/addFoodItemsButton";
import CreateRecipeButton from '../createRecipe/createRecipe'
import KitchenEquipmentButton from "../KitchenEquipment/addToKitchenEquipmentButton";
import LoginButton from './LoginButton'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    color: "black"
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));



const ButtonAppBar = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <KitchenEquipmentButton />
          <CreateRecipeButton />
          <FoodItemsButton />
          <LoginButton />
        </Toolbar>
      </AppBar>
    </div>
  );
}
export default ButtonAppBar;