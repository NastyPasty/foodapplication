import { AUTH_TOKEN } from '../Accounts/constants'
import { Button } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';


const LoginButton = () => {

  const authToken = localStorage.getItem(AUTH_TOKEN);
  return (
    <div className="flex flex-fixed">
      {authToken ? (
        <Button variant='contained'
          className="ml1 pointer black"
          onClick={() => {
            localStorage.removeItem(AUTH_TOKEN)

          }}
        >
          logout
        </Button>
      ) : (
          <Link component={Button} to="/login" className="ml1 no-underline black">
            login
          </Link>
        )}
    </div>
  )
}
export default LoginButton;