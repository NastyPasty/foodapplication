import React from "react";

const ReactHeader = (props) => {
    return (
        <h1>{props.headerValue}</h1>
    )
}

export default ReactHeader;