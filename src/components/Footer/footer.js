import React from 'react';
import Footer from 'rc-footer';
import 'rc-footer/assets/index.css'; // import 'rc-footer/asssets/index.less';
import styles from "./Footer.module.css"

const RecipeFooter = () => {

  return (
    
      <Footer
        maxColumnsPerRow={4}
        columns={[
          {
           
            title: 'Recipe Application',

            openExternal: true,
          },
          {
            title: "test"
          },
          {
            title: "test"
          },
          {
            title: "test"
          },

        ]}
        bottom="Made by Joe"
      />
   
  );
}

export default RecipeFooter;