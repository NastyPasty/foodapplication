import RecipeIngredientsReducer from './RecipeIngredientsReducer';
import RecipePageReducer from './RecipePageReducer';
import FoodUnitReducer from './FoodUnitReducer';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  servingIngredients: RecipeIngredientsReducer,
  recipePage: RecipePageReducer,
  unit: FoodUnitReducer
})


export default rootReducer;