import { RecipeName, RecipeImage, RecipeServings } from '../constants/constants'

const initialState = {
    recipeName: "",
    recipeImage: "",
    recipeServings: 0
}
function RecipePageReducer(state = initialState, action) {
    switch (action.type) {
        case RecipeName:
            return {
                ...state,

                recipeName: action.payload,
                loading: true
            }
        case RecipeImage:
            return {
                ...state,

                recipeImage: action.payload,
                loading: true
            }
        case RecipeServings:
            return {
                ...state,

                recipeServings: action.payload,
                loading: true
            }
        default:
            return state
    }
};


export default RecipePageReducer;