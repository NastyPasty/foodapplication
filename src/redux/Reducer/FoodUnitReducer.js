import { Unit } from '../constants/constants'

const initialState = {
    unit: ""
}
function FoodUnitReducer(state = initialState, action) {
    switch (action.type) {
        case Unit:
            return {
                ...state,

                unit: action.payload,
                loading: true
            }

        default:
            return state
    }
};


export default FoodUnitReducer;