import { Serving_Ingredients } from '../constants/constants';

const initialState = {
    servingIngredients: []
}
function servingListReducer(state = initialState, action) {
    switch (action.type) {
        case Serving_Ingredients:
            return Object.assign({}, state, {
                servingIngredients: state.servingIngredients.concat(action.payload),
                loading: true
            })
        case "Clear":
            return Object.assign({}, state, {
                servingIngredients: [],
                loading: true
            })
        default:
            return state
    }
}

export default servingListReducer;