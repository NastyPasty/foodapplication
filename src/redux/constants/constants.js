export const Serving_Ingredients = "servingIngredientList";
export const RecipeName = "recipeName";
export const RecipeImage = 'recipeImage';
export const RecipeServings = 'recipeSerings';
export const Unit = 'unit';
export const Clear = 'Clear';