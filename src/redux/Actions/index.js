import { Serving_Ingredients, RecipeName, RecipeServings, RecipeImage, Unit, Clear } from "../constants/constants"

function addservingIngredient(payload) {
    return {
        type: Serving_Ingredients,
        payload
    }
}
function resetServingIngredient() {
    return {
        type: Clear,

    }
}

function addRecipeName(payload) {
    return {
        type: RecipeName,
        payload
    }
}
function setRecipeName() {
    return (dispatch => {
        dispatch({ type: RecipeName, recipePage })
    })
}
function addRecipeImage(payload) {
    return {
        type: RecipeImage,
        payload
    }
}
function setRecipeImage() {
    return (dispatch => {
        dispatch({
            type: RecipeImage,
            recipePage
        })
    })
}
function addRecipeServing(payload) {
    return {
        type: RecipeServings,
        payload
    }
}
function setRecipeServing() {
    return (dispatch => {
        dispatch({
            type: RecipeServings,
            recipePage
        })
    })
}
function addUnit(payload) {
    return {
        type: Unit,
        payload
    }
}
function setUnit() {
    return (dispatch => {
        dispatch({
            type: Unit,
            recipePage
        })
    })
}
export {
    addservingIngredient,
    resetServingIngredient,
    addRecipeName,
    setRecipeName,
    addRecipeImage,
    setRecipeImage,
    addRecipeServing,
    setRecipeServing,
    addUnit,
    setUnit
}