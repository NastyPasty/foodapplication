const path = require("path");
const slsw = require("serverless-webpack");
const webpack = require('webpack');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  entry: slsw.lib.entries,
  target: 'node',
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        use: [
          { loader: 'ts-loader' }
        ]
      },
      {
        test: /\.json$/,
        use: [
          { loader: 'json' }
        ]
      },
      {
        test: /\.sql$/,
        use: [
          { loader: 'raw-loader' }
        ]
      },
      {
        test: /\.(md|jst|def)$/,
        use: [
          { loader: 'ignore-loader' }
        ]
      },
    ]
  },
  resolve: {
    extensions: ['.ts', '.js', '.tsx', '.jsx', '.json', '']
  },
  output: {
    libraryTarget: 'commonjs',
    path: path.join(__dirname, '.webpack'),
    filename: '[name].js',
    comments: false
  },
  plugins: [

  ]
};