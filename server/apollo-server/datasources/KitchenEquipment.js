const { KitchenEquipment, ObjectID } = require('../src/databaseVariables')
const { DataSource } = require('apollo-datasource');



class KitchenEquipmentAPI extends DataSource {
  constructor({ db }) {
    super();
    this.db = db;
    this.kitchenEquipmentReducer = this.kitchenEquipmentReducer.bind(this);
    this.getkitchenEquipmentById = this.getKitchenEquipmentById.bind(this);
    this.getAllkitchenEquipments = this.getAllKitchenEquipments.bind(this);
    this.getkitchenEquipmentsByIds = this.getKitchenEquipmentsByIds.bind(this);
  }

  async getAllKitchenEquipments() {
    const response = await this.db.collection(KitchenEquipment).find().toArray().then(res => { return res });
    console.log();
    return Array.isArray(response)
      ? response.map(kitchenEquipment => this.kitchenEquipmentReducer(kitchenEquipment))
      : [];

  }

  kitchenEquipmentReducer(kitchenEquipment) {

    return {
      id: kitchenEquipment._id || 0,
      kitchenEquipmentName: kitchenEquipment.kitchenEquipmentName,

    };
  }

  async getKitchenEquipmentById({ kitchenEquipmentId }) {
    const response = await this.db.collection(KitchenEquipment, { id: kitchenEquipmentId }).find().toArray().then(res => { return res });
    return this.kitchenEquipmentReducer(response[0]);
  }

  getKitchenEquipmentsByIds({ kitchenEquipmentIds }) {
    return Promise.all(
      kitchenEquipmentIds.map(kitchenEquipmentId => this.getKitchenEquipmentById({ kitchenEquipmentId })),
    );
  }
  async addKitchenEquipment({ kitchenEquipmentName }) {
    const createKitchenEquipment = this.db.collection(KitchenEquipment).insertOne({ kitchenEquipmentName });

    return createKitchenEquipment;
  }

  async deleteKitchenEquipment(id) {
    try {

      const deleteKitchenEquipment = await this.db.collection(KitchenEquipment, { _id: ObjectID(id) }).deleteOne({ _id: ObjectID(id) });
      return deleteKitchenEquipment;
    }
    catch (e) {
      print(e);
    }
  }
}




module.exports = KitchenEquipmentAPI;