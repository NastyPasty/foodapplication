
const { Users, DatabaseName, client, ObjectID } = require('../src/databaseVariables')
const { DataSource } = require('apollo-datasource');
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { APP_SECRET, getUserId } = require('../src/utils')




class UserAPI extends DataSource {
  constructor({ db }) {
    super();
    this.db = db;
  }

  async signup(email) {

    const salt = await bcrypt.genSaltSync(10);
    const cryptpassword = await bcrypt.hash(email.password, salt)
    const emailsave = email.email;
    const name = email.name;
    const user = await this.db.collection(Users).insertOne({ emailsave, name, cryptpassword });
    const token = await jwt.sign({ userId: user.id }, APP_SECRET)

    return {
      token,
      user,
    }
  }

  async  login(email) {

    const user = await this.db.collection(Users).find({ emailsave: email.email }).toArray().then(res => { return res });
    const password = email.password

    if (!user) {
      throw new Error('No such user found')
    }

    const valid = await bcrypt.compare(password, user[0].cryptpassword)
    if (!valid) {
      throw new Error('Invalid password')
    }

    return {
      token: jwt.sign({ userId: user.id }, APP_SECRET),
      user,
    }
  }

}






module.exports = UserAPI;