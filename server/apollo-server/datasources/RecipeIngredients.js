
const { RecipeIngredients, ObjectID } = require('../src/databaseVariables')
const { DataSource } = require('apollo-datasource');


class RecipeIngredientAPI extends DataSource {
  constructor({ db }) {
    super();
    this.db = db;
    this.recipeIngredientReducer = this.recipeIngredientReducer.bind(this);
    this.getRecipeIngredientById = this.getRecipeIngredientById.bind(this);
    this.getAllRecipeIngredients = this.getAllRecipeIngredients.bind(this);
    this.getRecipeIngredientsByIds = this.getRecipeIngredientsByIds.bind(this);
  }
  async getAllRecipeIngredients() {
    const response = await this.db.collection(RecipeIngredients).find().toArray().then(res => { return res });

    return Array.isArray(response)
      ? response.map(recipeIngredient => this.recipeIngredientReducer(recipeIngredient))
      : [];

  }

  recipeIngredientReducer(recipeIngredient) {

    return {
      id: recipeIngredient._id || 0,
      recipeID: recipeIngredient.recipeID,
      ingredientName: recipeIngredient.ingredientName,
      ingredientQuantity: recipeIngredient.ingredientQuantity,
      ingredientUnit: recipeIngredient.ingredientUnit,
    };
  }

  async getRecipeIngredientById({ recipeIngredientId }) {
    const response = await this.db.collection(RecipeIngredients, { id: recipeIngredientId }).find().toArray().then(res => { return res });
    return this.recipeIngredientReducer(response[0]);
  }

  async getRecipeIngredientByRecipeId({ recipeID }) {
    const response = await this.db.collection(RecipeIngredients, { recipeID: recipeID }).find({ recipeID: recipeID }).toArray().then(res => { return res });

    return Array.isArray(response)
      ? response.map(recipeIngredient => this.recipeIngredientReducer(recipeIngredient))
      : [];
  }


  getRecipeIngredientsByIds({ recipeIngredientIds }) {
    return Promise.all(
      recipeIngredientIds.map(recipeIngredientId => this.getrecipeIngredientById({ recipeIngredientId })),
    );
  }

  getRecipeIngredientsByRecipeIds({ recipeIds }) {

    return Promise.all(
      recipeIds.map(recipeID => this.getrecipeIngredientByRecipeId({ recipeID })),
    );
  }

  async addRecipeIngredient({ recipeID, ingredientName, ingredientQuantity, ingredientUnit }) {
    const createRecipeIngredient = this.db.collection(RecipeIngredients).insertOne({ recipeID, ingredientName, ingredientUnit, ingredientQuantity });

    return createRecipeIngredient;
  }

  async deleteRecipeIngredient(id) {
    const deleteRecipeIngredient = await this.db.collection(RecipeIngredients).deleteOne({ _id: ObjectID(id) });

    return deleteRecipeIngredient;
  }

  async updateIngredient({ id, ingredientName, ingredientUnit, ingredientQuantity }) {
    const updateIngredient = await this.db.collection(RecipeIngredients).updateOne({ _id: ObjectID(id) }, {
      $set: {
        ingredientQuantity: ingredientQuantity,
        ingredientUnit: ingredientUnit,
        ingredientName: ingredientName
      }
    });

    return updateIngredient
  }
}




module.exports = RecipeIngredientAPI;