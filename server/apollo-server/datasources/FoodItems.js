const { FoodItems, ObjectID } = require('../src/databaseVariables')
const { DataSource } = require('apollo-datasource');



class FoodItemAPI extends DataSource {
  constructor({ db }) {
    super();
    this.db = db;
    this.foodItemReducer = this.foodItemReducer.bind(this);
    this.getFoodItemById = this.getFoodItemById.bind(this);
    this.getAllFoodItems = this.getAllFoodItems.bind(this);
    this.getFoodItemsByIds = this.getFoodItemsByIds.bind(this);
  }
  async getAllFoodItems() {
    const response = await this.db.collection(FoodItems).find().toArray().then(res => { return res });

    return Array.isArray(response)
      ? response.map(foodItem => this.foodItemReducer(foodItem))
      : [];

  }

  async getSingleRecipeFoodItems(nameOfItem) {
    const response = await this.db.collection(FoodItems)
      .find({ nameOfItem: nameOfItem }).toArray().then(res => { return res });
    console.log(this.foodItemReducer(response[0]));
    return this.foodItemReducer(response[0]);
  }

  foodItemReducer(foodItem) {

    return {
      id: foodItem._id || 0,
      nameOfItem: foodItem.nameOfItem,
      expirationDate: foodItem.expirationDate,
      weightOfItem: foodItem.weightOfItem,
      unitOfMeasurement: foodItem.unitOfMeasurement,

    };
  }

  async getFoodItemById({ foodItemId }) {
    const response = await this.db.collection(FoodItems, { id: foodItemId }).find().toArray().then(res => { return res });
    return this.foodItemReducer(response[0]);
  }

  getFoodItemsByIds({ foodItemIds }) {
    return Promise.all(
      foodItemIds.map(foodItemId => this.getFoodItemById({ foodItemId })),
    );
  }

  async addFoodItem({ nameOfItem, expirationDate, weightOfItem, unitOfMeasurement }) {
    const createFoodItems = this.db.collection(FoodItems).insertOne({ nameOfItem, expirationDate, weightOfItem, unitOfMeasurement });

    return createFoodItems;
  }

  async deleteFoodItem(id) {
    const deleteFoodItems = await this.db.collection(FoodItems, { id: id }).deleteOne({ _id: ObjectID(id) });
    return deleteFoodItems;
  }

  async updateFoodItem({ id, nameOfItem, weightOfItem, unitOfMeasurement, expirationDate }) {
    const updateFoodItem = await db.collection(FoodItems).updateOne({ _id: ObjectID(id) }, {
      $set: {
        nameOfItem: nameOfItem,
        weightOfItem: weightOfItem,
        unitOfMeasurement: unitOfMeasurement,
        expirationDate, expirationDate
      }
    });

    return updateFoodItem
  }

  async completedFoodItem({ nameOfItem, weightOfItem, orignalWeight }) {
    const UpdatedWeight = orignalWeight - weightOfItem;
    const updateFoodItem = await db.collection(FoodItems).updateOne({ nameOfItem: nameOfItem }, {
      $set: {
        weightOfItem: UpdatedWeight,
      }
    });

    return updateFoodItem
  }
}




module.exports = FoodItemAPI;