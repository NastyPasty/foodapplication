const { RecipeNames, DatabaseName, client, ObjectID } = require('../src/databaseVariables')
const { DataSource } = require('apollo-datasource')



class RecipeNameAPI extends DataSource {
  constructor({ db }) {
    super();
    this.db = db
    this.recipeNameReducer = this.recipeNameReducer.bind(this);
    this.getRecipeNameById = this.getRecipeNameById.bind(this);
    this.getAllRecipeNames = this.getAllRecipeNames.bind(this);
    this.getRecipeNamesByIds = this.getRecipeNamesByIds.bind(this);
  }
  async getAllRecipeNames() {
    const response = await this.db.collection(RecipeNames).find().toArray().then(res => { return res });

    return Array.isArray(response)
      ? response.map(recipeName => this.recipeNameReducer(recipeName))
      : [];

  }

  recipeNameReducer(recipeName) {

    return {
      id: recipeName._id || 0,
      recipeid: recipeName.recipeid,
      recipeName: recipeName.recipeName,
      timeToCook: recipeName.timeToCook,
      prepTime: recipeName.prepTime,
      totalTime: recipeName.totalTime,
      servings: recipeName.servings,
      recipeTag: recipeName.recipeTag,
      file: recipeName.file || null
    };
  }

  async getRecipeNameById({ recipeNameId }) {
    const response = await this.db.collection(RecipeNames, { id: recipeNameId }).find().toArray().then(res => { return res });

    return this.recipeNameReducer(response[0]);
  }

  getRecipeNamesByIds({ recipeNameIds }) {
    return Promise.all(
      recipeNameIds.map(recipeNameId => this.getRecipeNameById({ recipeNameId })),
    );
  }

  async addRecipeName({ recipeid,
    recipeName, prepTime, timeToCook, totalTime, servings, recipeTag, file }) {
    const createRecipeName = this.db.collection(RecipeNames).insertOne({ recipeid, recipeName, prepTime, timeToCook, totalTime, servings, recipeTag, file });

    return createRecipeName;
  }

  async deleteRecipeName(recipeID) {
    const deleteRecipeName = await this.db.collection(RecipeNames, { id: recipeID }).deleteOne({ _id: ObjectID(recipeID) });
    return deleteRecipeName;
  }
}




module.exports = RecipeNameAPI;