const { RecipeSteps, ObjectID } = require('../src/databaseVariables')
const { DataSource } = require('apollo-datasource');




class RecipeStepAPI extends DataSource {
  constructor({ db }) {
    super();

    this.db = db;
    this.recipeStepReducer = this.recipeStepReducer.bind(this);
    this.getRecipeStepById = this.getRecipeStepById.bind(this);
    this.getAllRecipeSteps = this.getAllRecipeSteps.bind(this);
    this.getRecipeStepsByIds = this.getRecipeStepsByIds.bind(this);
  }
  async getAllRecipeSteps() {
    const response = await this.db.collection(RecipeSteps).find().toArray().then(res => { return res });

    return Array.isArray(response)
      ? response.map(recipeStep => this.recipeStepReducer(recipeStep))
      : [];

  }

  recipeStepReducer(recipeStep) {

    return {
      id: recipeStep._id || 0,
      recipeID: recipeStep.recipeID,
      stepNumber: recipeStep.stepNumber,
      stepInformation: recipeStep.stepInformation,
    };
  }

  async getRecipeStepById({ recipeStepId }) {
    const response = await this.db.collection(RecipeSteps, { id: recipeStepId }).find().toArray().then(res => { return res });
    return this.recipeStepReducer(response[0]);
  }

  async getRecipeStepByRecipeId({ recipeID }) {
    const response = await this.db.collection(RecipeSteps).find({ recipeID: recipeID }).toArray().then(res => { return res });

    return Array.isArray(response)
      ? response.map(recipeStep => this.recipeStepReducer(recipeStep))
      : [];
  }


  getRecipeStepsByIds({ recipeStepIds }) {
    return Promise.all(
      recipeStepIds.map(recipeStepId => this.getRecipeStepById({ recipeStepId })),
    );
  }

  getRecipeStepsByRecipeIds({ recipeIds }) {

    return Promise.all(
      recipeIds.map(recipeID => this.getRecipeStepByRecipeId({ recipeID })),
    );
  }

  async addRecipeStep({ recipeID, stepNumber, stepInformation }) {
    const createRecipeStep = this.db.collection(RecipeSteps).insertOne({ recipeID, stepNumber, stepInformation });

    return createRecipeStep;
  }

  async deleteRecipeStep(recipeID) {
    const deleteRecipeStep = await this.db.collection(RecipeSteps).deleteOne({ _id: ObjectID(recipeID) });
    return deleteRecipeStep;
  }

  async updateRecipeStep({ id, stepInformation }) {
    const updateRecipeStep = await this.db.collection(RecipeSteps).updateOne({ _id: ObjectID(id) }, { $set: { stepInformation: stepInformation } });

    return updateRecipeStep
  }
}






module.exports = RecipeStepAPI;