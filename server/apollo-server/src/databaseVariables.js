const MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
const RecipeSteps = 'recipesteps'
const RecipeIngredients = 'recipeIngredients'
const KitchenEquipment = 'kitchenequipments'
const RecipeNames = 'recipenames'
const FoodItems = 'fooditems'
const Users = 'users'
const DatabaseName = 'foodApp'
const url = 'mongodb://localhost/foodApp';

const client = new MongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true });


module.exports = {
    RecipeSteps,
    RecipeIngredients,
    KitchenEquipment,
    FoodItems,
    RecipeNames,
    Users,
    DatabaseName,
    url,
    client,
    ObjectID
};