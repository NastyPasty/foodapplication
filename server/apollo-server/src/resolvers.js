module.exports = {
  Query: {
    recipeNames: async (_, __, { dataSources }) => {
      const allRecipeNames = await dataSources.recipeNameAPI.getAllRecipeNames();

      return allRecipeNames;
    },

    recipeName: (_, { id }, { dataSources }) => {
      dataSources.recipeNameAPI.getRecipeNameById({ recipeNameId: id })
    },

    kitchenEquipments: async (_, __, { dataSources }) => {
      const allKitchenEquipment = await dataSources.kitchenEquipmentAPI.getAllKitchenEquipments();

      return allKitchenEquipment;
    },

    foodItems: async (_, __, { dataSources }) => {
      const allFooditems = await dataSources.foodItemAPI.getAllFoodItems();

      return allFooditems;
    },
    singleRecipeFoodItems: async (_, { nameOfItem }, { dataSources }) => {
      const recipeFoodItems = await dataSources.foodItemAPI.getSingleRecipeFoodItems(nameOfItem);

      return recipeFoodItems
    },
    recipeIngredients: async (_, { recipeID }, { dataSources }) => {
      const recipeIngredients = await dataSources.recipeIngredientAPI.getRecipeIngredientByRecipeId({ recipeID: recipeID })

      return recipeIngredients
    },

    recipeSteps: async (_, { recipeID }, { dataSources }) => {
      const recipeSteps = await dataSources.recipeStepAPI.getRecipeStepByRecipeId({ recipeID: recipeID })

      return recipeSteps
    },
  },

  Mutation: {
    login: async (__, { email, password }, { dataSources }) => {

      const login = await dataSources.UserAPI.login({ email, password });
      return login;
    },

    signup: async (__, { email, password, name }, { dataSources }) => {

      const signup = await dataSources.UserAPI.signup({ email, password, name });

      return signup;
    },

    addRecipeName: async (__, { recipeid, recipeName, timeToCook, prepTime, totalTime, servings, recipeTag, file }, { dataSources }) => {
      const PostedrecipeName = await dataSources.recipeNameAPI.addRecipeName({ recipeid, recipeName, timeToCook, prepTime, totalTime, servings, recipeTag, file });
      const allRecipeNames = await dataSources.recipeNameAPI.getAllRecipeNames();

      return allRecipeNames
    },

    addRecipeStep: async (_, { recipeID, stepNumber, stepInformation }, { dataSources }) => {
      const PostedrecipeStep = await dataSources.recipeStepAPI.addRecipeStep({ recipeID, stepNumber, stepInformation });
      const allRecipeSteps = await dataSources.recipeStepAPI.getAllRecipeSteps();

      return allRecipeSteps
    },

    addIngredients: async (_, { recipeID, ingredientName, ingredientUnit, ingredientQuantity }, { dataSources }) => {
      const PostedrecipeIngredient = await dataSources.recipeIngredientAPI.addRecipeIngredient({ recipeID, ingredientName, ingredientUnit, ingredientQuantity });
      const allRecipeIngredients = await dataSources.recipeIngredientAPI.getAllRecipeIngredients();

      return allRecipeIngredients
    },

    addFoodItem: async (_, { nameOfItem, expirationDate, weightOfItem, unitOfMeasurement }, { dataSources }) => {
      const PostedFoodItem = await dataSources.foodItemAPI.addFoodItem({ nameOfItem, expirationDate, weightOfItem, unitOfMeasurement });
      const allFoodItems = await dataSources.foodItemAPI.getAllFoodItems();

      return allFoodItems
    },

    addKitchenEquipment: async (_, { recipeID, kitchenEquipmentName }, { dataSources }) => {
      const PostedKitchenEquipment = await dataSources.kitchenEquipmentAPI.addKitchenEquipment({ recipeID, kitchenEquipmentName });
      const allKitchenEquipments = await dataSources.kitchenEquipmentAPI.getAllKitchenEquipments();

      return allKitchenEquipments;
    },

    updateRecipeStep: async (_, { id, stepInformation }, { dataSources }) => {
      const updateRecipeStep = await dataSources.recipeStepAPI.updateRecipeStep({ id, stepInformation });
      const allRecipeSteps = await dataSources.recipeStepAPI.getAllRecipeSteps();

      return allRecipeSteps
    },

    updateIngredient: async (_, { id, ingredientUnit, ingredientQuantity, ingredientName }, { dataSources }) => {
      const updateIngredient = await dataSources.recipeIngredientAPI.updateIngredient({ id, ingredientUnit, ingredientQuantity, ingredientName });
      const allIngredients = await dataSources.recipeIngredientAPI.getAllRecipeIngredients();

      return allIngredients
    },
    updateFoodItem: async (_, { id, nameOfItem, weightOfItem, unitOfMeasurement, expirationDate }, { dataSources }) => {
      const updateFoodItem = await dataSources.foodItemAPI.updateFoodItem({ id, nameOfItem, weightOfItem, unitOfMeasurement, expirationDate });
      const allIngredients = await dataSources.foodItemAPI.getAllFoodItems();

      return allIngredients
    },
    completedFoodItem: async (_, { id, nameOfItem, weightOfItem, orignalWeight }, { dataSources }) => {
      const updateFoodItem = await dataSources.foodItemAPI.completedFoodItem({ id, nameOfItem, weightOfItem, orignalWeight });

    },
    removeRecipeName: async (_, { id }, { dataSources }) => {
      const removeRecipeName = dataSources.recipeNameAPI.deleteRecipeName(id);
      const allRecipeNames = await dataSources.recipeNameAPI.getAllRecipeNames();

      return allRecipeNames;
    },

    removeRecipeStep: async (_, { id }, { dataSources }) => {
      const removeRecipeName = dataSources.recipeStepAPI.deleteRecipeStep(id);
      const allRecipeSteps = await dataSources.recipeStepAPI.getAllRecipeSteps();

      return allRecipeSteps;
    },

    removeIngredients: async (_, { id }, { dataSources }) => {
      const removeRecipeName = dataSources.recipeIngredientAPI.deleteRecipeIngredient(id);
      const allRecipeIngredients = await dataSources.recipeIngredientAPI.getAllRecipeIngredients();

      return allRecipeIngredients;
    },

    removeFoodItem: async (_, { id }, { dataSources }) => {
      const removeFooditem = dataSources.foodItemAPI.deleteFoodItem(id);
      const allFoodItemss = await dataSources.foodItemAPI.getAllFoodItems();

      return allFoodItemss;
    },

    removeKitchenEquipment: async (_, { id }, { dataSources }) => {

      const removeKitchenEquipment = dataSources.kitchenEquipmentAPI.deleteKitchenEquipment(id);
      const allKitchenEquipments = await dataSources.kitchenEquipmentAPI.getAllKitchenEquipments();

      return allKitchenEquipments;
    },

  }
};