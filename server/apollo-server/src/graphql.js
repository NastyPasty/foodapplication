const { ApolloServer, gql } = require("apollo-server-lambda");
const typeDefs = require('./schema');
const resolvers = require('./resolvers');
const { DatabaseName, client } = require('./databaseVariables');


const RecipeNameAPI = require('../datasources/RecipeNames')
const RecipeStepAPI = require('../datasources/RecipeSteps');
const RecipeIngredientAPI = require('../datasources/RecipeIngredients');
const KitchenEquipmentAPI = require('../datasources/KitchenEquipment');
const FoodItemAPI = require('../datasources/FoodItems');
const UserAPI = require('../datasources/User')

client.connect(function (err) {
  console.log("MONGOdb connected");
  db = client.db(DatabaseName); //mongodb database name
});


const server = new ApolloServer({
  typeDefs,
  resolvers,
  path: "/graphql",
  dataSources: () => ({
    recipeNameAPI: new RecipeNameAPI({ db }),
    recipeStepAPI: new RecipeStepAPI({ db }),
    recipeIngredientAPI: new RecipeIngredientAPI({ db }),
    kitchenEquipmentAPI: new KitchenEquipmentAPI({ db }),
    foodItemAPI: new FoodItemAPI({ db }),
    UserAPI: new UserAPI({ db }),

  })
});

module.exports.graphqlHandler = server.createHandler({
  cors: {
    origin: '*',
    credentials: true,
  },
});