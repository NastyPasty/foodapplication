const { gql } = require('apollo-server-lambda');

const typeDefs = gql`
scalar Date
scalar Image

type User{
    id: ID!
    email: String
    password: String
    name: String
}


type AuthPayload {
    token: String
    user: User
  }
type RecipeName {
    id: ID!
    recipeid: String
    recipeName: String
    timeToCook: Int
    prepTime: Int
    totalTime: Int
    servings: Int
    recipeTag: String
    file: String

    }

    type RecipeIngredient {
    id:ID!
    recipeID: String!
    ingredientQuantity: String
    ingredientUnit: String
    ingredientName: String
    }

    type RecipeStep {
    id :ID!
    recipeID: String!
    stepNumber: Int
    stepInformation: String
    }

    type KitchenEquipment{
    id :ID!
    kitchenEquipmentName: String
    }

    type FoodItem {
    id:ID!
    nameOfItem: String
    expirationDate: Date
    weightOfItem: Int
    unitOfMeasurement: String
    }

    type Query{
        recipeNames: [RecipeName]!

        recipeName(id: ID!): RecipeName 

        recipeIngredients(recipeID: String):[RecipeIngredient]

        recipeSteps(recipeID: String): [RecipeStep]

        kitchenEquipments: [KitchenEquipment]!
        
        foodItems: [FoodItem]!
        
        singleRecipeFoodItems(nameOfItem: String): FoodItem
        }
        
type Mutation{
   signup(
       email: String!,
       password: String!,
       name: String!
   ): AuthPayload

    login(
        email: String!,
       password: String!,
    ): AuthPayload
    
    addFoodItem(
        nameOfItem: String,
        expirationDate: Date,
        weightOfItem: Int,
        unitOfMeasurement: String): [FoodItem]

    addIngredients(recipeID: String!,
      ingredientQuantity: String,
      ingredientUnit: String,
      ingredientName: String ): [RecipeIngredient]

    addKitchenEquipment( kitchenEquipmentName: String  ): [KitchenEquipment]

    addRecipeStep(recipeID: String!,
        stepNumber: Int ,
        stepInformation: String): [RecipeStep]

    addRecipeName( recipeid: String!,
        recipeName: String!,
        timeToCook: Int,
        prepTime: Int,
        totalTime: Int,
        servings: Int,
        recipeTag: String,
        file: Upload
        ): [RecipeName]

    removeFoodItem(id: String): [FoodItem]

    removeIngredients(id: String): [RecipeIngredient]

    removeKitchenEquipment(id: String): [KitchenEquipment]

    removeRecipeStep(id: String): [RecipeStep]

    removeRecipeName(id: String): [RecipeName]

    updateIngredient(id: String!,
        ingredientQuantity: String,
        ingredientUnit: String ,
        ingredientName: String ): [RecipeIngredient]

    updateRecipeStep(id: String!,
        stepInformation: String): [RecipeStep]

    updateFoodItem(id: String!,
        nameOfItem: String,
        weightOfItem: Int,
        unitOfMeasurement: String,
        expirationDate: Date): [FoodItem]

        completedFoodItem(
            nameOfItem: String!,
            weightOfItem: Int,
            orignalWeight: Int!,
            unitOfMeasurement: String): [FoodItem]
}

`;


module.exports = typeDefs;