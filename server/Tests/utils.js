const { HttpLink } = require('apollo-link-http');
const fetch = require('node-fetch');
const { execute, toPromise } = require('apollo-link');


module.exports.toPromise = toPromise;

const {
  dataSources,
  context: defaultContext,
  typeDefs,
  resolvers,
  ApolloServer,
  UserAPI,
  store,
} = require('../');

/**
 * Integration testing utils
 */
const constructTestServer = ({ context = defaultContext } = {}) => {
  const userAPI = new UserAPI({ store });


  const server = new ApolloServer({
    typeDefs,
    resolvers,
    dataSources: () => ({ userAPI }),
    context,
  });

  return { server, userAPI };
};

module.exports.constructTestServer = constructTestServer;
